#!/usr/bin/env bash

getent passwd | while IFS=':' read login _ uid gid gecos home shell
do
    echo "NAME=${login} UID=${uid} HOME=${home}"
done
