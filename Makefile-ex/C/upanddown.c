void slower(char *s) {
	while (*s) {
		if (*s >= 'A' && *s <= 'Z') {
			*s += 32;
		}
	s++;
	}
}

void supper(char *s) {
	while (*s) {
		if (*s >= 'a' && *s <= 'z') {
			*s -= 32;
		}
	s++;
	}
}
