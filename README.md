# Formation Linux/Bash Avril 2024

Si vous voulez cloner ce dépot sur vos systèmes :

~~~~Bash
$ git clone https://gitlab.com/python_431/m2iluxsh1.git
$ cd m2iluxsh1
$ ls
...
~~~~

Pour le mettre à jour :

~~~~Bash
$ cd ~/m2iluxsh1
$ git pull
$ ls
~~~~
