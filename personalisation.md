# qq personnalisation du Shell

Dans mon `~root/.bashrc` :

~~~~Bash
export PS1="\[\e[31m\]\u\[\e[m\]@\[\e[32m\]\h\[\e[m\]:\[\e[34m\]\w\[\e[m\]\[\e[31m\]\\$\[\e[m\] "
~~~~
